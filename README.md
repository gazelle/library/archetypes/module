# Maven Module Archetype

This module defines an archetype to instantiate a new module with proper Gazelle structure to include in any Gazelle project.

## Goals

- Generate a new module in the current project with a pom file valorized with the right identifiers.
- Generate the adapter/application/business packages with the path : net.ihe.gazelle.**prefix**.**packageName** populated with readme and their test counterparts.


## Properties

Here is the list of properties that can be used to generate a module with this Archetype :

| Property Name | Default Value   | Usage                                                                                                                                            |
|---------------|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| groupId       | net.ihe.gazelle | Group ID for maven pom.xml                                                                                                                       |
| artifactId    | my-module       | Part of maven pom.xml artifactId (will be prefix.**artifactId**). Also used as folder name for your new module                                   |
| parentId      | my-parent       | Part of maven pom.xml as parent artifactId.                                                                                                      |
| prefix        | lib             | Prefix for the module. Will be used in maven artifactId (will be **prefix**.artifactId). Also used to name package at net.ihe.gazelle.**prefix** |
| packageName   | mymodule        | Package name used at net.ihe.gazelle.prefix.**packageName**                                                                                      |  
| projectName   | My Module       | Project Name for the maven module.                                                                                                               | 
| version       | 1.0.0-SNAPSHOT  | Version of the module, and of its parent.                                                                                                        |


## How to generate a new Module

To generate a new module with this archetype, go to the target folder of your choice :

```cd my/target/folder```

Then use the following command to generate your module :

```mvn archetype:generate -DarchetypeGroupId=net.ihe.gazelle -DarchetypeArtifactId=arch.module -DarchetypeVersion=1.0.0 -DgroupId=net.ihe.gazelle -Dprefix=lib -DartifactId=my-lib -DparentId=lib.my-parent -DpackageName=mylib  -DprojectName="My Fabulous Library" -Dversion=2.0.0-SNAPSHOT ```

First 3 parameters are mandatory to specify which archetype you are using and in which version.
Following parameters are optional, as default values are defined. If they are not defined in the command, default values will be used.
